package com.earth.parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import com.earth.dto.IndexPair;
import com.earth.dto.Input;

/**
 * This is the main class for Alphabet Soup project.
 * 
 * @author zengxi zhou
 *
 */
public class Driver {
	static final int DIRECTION_HORIZONTAL = 0;
	static final int DIRECTION_NW2SE = 1;
	static final int DIRECTION_VERTICAL = 2;
	static final int DIRECTION_NE2SW = 3;

	static Input input = new Input();

	/**
	 * Used to store the current search word to cover cases where search word has a
	 * space in it, i.e., a phrase like "HAVE FUN"
	 */
	static String placeHolder = null;

	public static void main(String[] args) {
		if (args.length != 1) {
			usage();
			System.exit(10);
		}
		initialize(args[0]);
		buildCache();

		for (int i = 0; i < input.words.size(); i++) {
			placeHolder = input.words.get(i);
			String needle = input.words.get(i).replaceAll("\\s", "");
			// System.out.println("needle=" + needle);

			// DIRECTION_HORIZONTAL
			for (int k = 0; k < input.puzzleCache[DIRECTION_HORIZONTAL].size(); k++) {
				search(needle, input.puzzleCache[DIRECTION_HORIZONTAL].get(k),
						input.locationCache[DIRECTION_HORIZONTAL].get(k), DIRECTION_HORIZONTAL, false);
				/**
				 * reverse search
				 */
				search(new StringBuffer(needle).reverse().toString(), input.puzzleCache[DIRECTION_HORIZONTAL].get(k),
						input.locationCache[DIRECTION_HORIZONTAL].get(k), DIRECTION_HORIZONTAL, true);
			}

			// DIRECTION_NW2SE
			for (int k = 0; k < input.puzzleCache[DIRECTION_NW2SE].size(); k++) {
				search(needle, input.puzzleCache[DIRECTION_NW2SE].get(k), input.locationCache[DIRECTION_NW2SE].get(k),
						DIRECTION_NW2SE, false);
				search(new StringBuffer(needle).reverse().toString(), input.puzzleCache[DIRECTION_NW2SE].get(k),
						input.locationCache[DIRECTION_NW2SE].get(k), DIRECTION_NW2SE, true);
			}

			// DIRECTION_VERTICAL
			for (int k = 0; k < input.puzzleCache[DIRECTION_VERTICAL].size(); k++) {
				search(needle, input.puzzleCache[DIRECTION_VERTICAL].get(k),
						input.locationCache[DIRECTION_VERTICAL].get(k), DIRECTION_VERTICAL, false);
				search(new StringBuffer(needle).reverse().toString(), input.puzzleCache[DIRECTION_VERTICAL].get(k),
						input.locationCache[DIRECTION_VERTICAL].get(k), DIRECTION_VERTICAL, true);
			}

			// DIRECTION_NE2SW
			for (int k = 0; k < input.puzzleCache[DIRECTION_NE2SW].size(); k++) {
				search(needle, input.puzzleCache[DIRECTION_NE2SW].get(k), input.locationCache[DIRECTION_NE2SW].get(k),
						DIRECTION_NE2SW, false);
				search(new StringBuffer(needle).reverse().toString(), input.puzzleCache[DIRECTION_NE2SW].get(k),
						input.locationCache[DIRECTION_NE2SW].get(k), DIRECTION_NE2SW, true);
			}
		}
	}

	private static void usage() {
		System.out.println("Required input file missing. Usage: java com.earth.parser.Drive <inputFile>");
	}

	public static void initialize(String filePath) {
		// You may assume that the input files are correctly formatted. Error handling
		// for invalid input files may be ommitted.
		// read and parse the input file
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(filePath));

			String line = br.readLine();
			// first line
			if (line != null) {
				String[] temp = line.split("x");
				input.rowSize = Integer.parseInt(temp[0]);
				input.colSize = Integer.parseInt(temp[1]);

			} else {
			}

			// == puzzle grid section
			input.puzzle = new char[input.rowSize][input.colSize];
			int rowIndex = 0;
			line = br.readLine();
			while (line != null) {
				// remove spaces
				String s = line.replaceAll("\\s", "");
				for (int colIndex = 0; colIndex < s.length(); colIndex++) {
					input.puzzle[rowIndex][colIndex] = s.charAt(colIndex);
				}
				rowIndex++;
				line = br.readLine();
				if (rowIndex == input.rowSize) {
					break;
				}
			}

			// ==== "words to be found" section
			input.words = new ArrayList<String>();
			while (line != null) {
				input.words.add(line.toUpperCase());
				line = br.readLine();
			}

		} catch (Exception e) {
			System.out.println("input file parsing error:" + e.getMessage());

		} finally {
			try {
				br.close();
			} catch (Exception e) {
				//
			}
		}
	}

	/**
	 * Build cache storages in four directions, horizontal, vertical and two
	 * diagonal
	 */
	static void buildCache() {
		/**
		 * cache for horizontal rows
		 */
		input.puzzleCache[DIRECTION_HORIZONTAL] = new ArrayList<String>();
		input.locationCache[DIRECTION_HORIZONTAL] = new ArrayList<IndexPair>();

		for (int i = 0; i < input.rowSize; i++) {
			StringBuffer sb = new StringBuffer();
			for (int j = 0; j < input.colSize; j++) {
				sb.append(input.puzzle[i][j]);
			}
			// System.out.println(sb.toString());
			input.puzzleCache[DIRECTION_HORIZONTAL].add(sb.toString().toUpperCase());
			input.locationCache[DIRECTION_HORIZONTAL].add(new IndexPair(i, 0));
		}

		/**
		 * cache for vertical rows
		 */
		input.puzzleCache[DIRECTION_VERTICAL] = new ArrayList<String>();
		input.locationCache[DIRECTION_VERTICAL] = new ArrayList<IndexPair>();

		for (int j = 0; j < input.colSize; j++) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < input.rowSize; i++) {
				sb.append(input.puzzle[i][j]);
			}
			input.puzzleCache[DIRECTION_VERTICAL].add(sb.toString().toUpperCase());
			input.locationCache[DIRECTION_VERTICAL].add(new IndexPair(0, j));
		}

		/**
		 * cache for DIRECTION_NW2SE
		 */
		input.puzzleCache[DIRECTION_NW2SE] = new ArrayList<String>();
		input.locationCache[DIRECTION_NW2SE] = new ArrayList<IndexPair>();

		for (int j = 0; j < input.colSize; j++) {
			StringBuffer sb = new StringBuffer();
			int rowIndexStart = 0;
			int colIndexStart = j;
			while (true) {
				sb.append(input.puzzle[rowIndexStart][colIndexStart]);
				rowIndexStart++;
				colIndexStart++;
				if (rowIndexStart >= input.rowSize || colIndexStart >= input.colSize) {
					break;
				}
			}
			input.puzzleCache[DIRECTION_NW2SE].add(sb.toString().toUpperCase());
			input.locationCache[DIRECTION_NW2SE].add(new IndexPair(0, j));
		}

		for (int i = 1; i < input.rowSize; i++) {
			StringBuffer sb = new StringBuffer();
			int rowIndexStart = i;
			int colIndexStart = 0;
			while (true) {
				sb.append(input.puzzle[rowIndexStart][colIndexStart]);
				rowIndexStart++;
				colIndexStart++;
				if (rowIndexStart >= input.rowSize || colIndexStart >= input.colSize) {
					break;
				}
			}
			input.puzzleCache[DIRECTION_NW2SE].add(sb.toString().toUpperCase());
			input.locationCache[DIRECTION_NW2SE].add(new IndexPair(i, 0));
		}

		// System.out.println("input.puzzleCache[DIRECTION_NW2SE] size=" +
		// input.puzzleCache[DIRECTION_NW2SE].size());

		/**
		 * cache for DIRECTION_NE2SW
		 */
		input.puzzleCache[DIRECTION_NE2SW] = new ArrayList<String>();
		input.locationCache[DIRECTION_NE2SW] = new ArrayList<IndexPair>();

		for (int j = 0; j < input.colSize; j++) {
			StringBuffer sb = new StringBuffer();
			int rowIndexStart = 0;
			int colIndexStart = j;
			while (true) {
				sb.append(input.puzzle[rowIndexStart][colIndexStart]);
				rowIndexStart++;
				colIndexStart--;
				if (rowIndexStart >= input.rowSize || colIndexStart < 0) {
					break;
				}
			}
			input.puzzleCache[DIRECTION_NE2SW].add(sb.toString().toUpperCase());
			input.locationCache[DIRECTION_NE2SW].add(new IndexPair(0, j));
		}

		for (int i = 1; i < input.rowSize; i++) {
			StringBuffer sb = new StringBuffer();
			int rowIndexStart = i;
			int colIndexStart = input.colSize - 1;
			while (true) {
				sb.append(input.puzzle[rowIndexStart][colIndexStart]);
				rowIndexStart++;
				colIndexStart--;
				if (rowIndexStart >= input.rowSize || colIndexStart < 0) {
					break;
				}
			}
			input.puzzleCache[DIRECTION_NE2SW].add(sb.toString().toUpperCase());
			input.locationCache[DIRECTION_NE2SW].add(new IndexPair(i, input.colSize - 1));
		}
	}

	/**
	 * Checks if the needle is in a stack; a two-direction search
	 * 
	 * @param needle
	 * @param hayStack
	 */
	public static void search(String needle, String stack, IndexPair startingLocation, int direction,
			boolean isReversed) {
		// System.out.println(stack + " direction= " + direction + " " +
		// startingLocation.rowIndex + ":"
		// + startingLocation.colIndex);
		if (stack.length() < needle.length()) {
			return;
		}

		for (int i = 0; i <= (stack.length() - needle.length()); i++) {
			// System.out.println("i=" + i);

			int index = stack.indexOf(needle, i);
			if ((index - i) == 0) {

				// System.out.println("!! found " + index);
				output(needle, index, startingLocation, direction, isReversed);

			}
		}

	}

	/**
	 * 
	 * @param needle
	 * @param index
	 * @param startingLocation
	 * @param direction
	 * @param isRerved
	 */
	public static void output(String needle, int index, IndexPair startingLocation, int direction, boolean isReversed) {
		IndexPair start = null;
		IndexPair end = null;

		if (direction == DIRECTION_HORIZONTAL) {
			start = new IndexPair(startingLocation.rowIndex, startingLocation.colIndex + index);
			end = new IndexPair(startingLocation.rowIndex, startingLocation.colIndex + index + needle.length() - 1);

		} else if (direction == DIRECTION_VERTICAL) {
			start = new IndexPair(startingLocation.rowIndex + index, startingLocation.colIndex);
			end = new IndexPair(startingLocation.rowIndex + index + needle.length() - 1, startingLocation.colIndex);
		} else if (direction == DIRECTION_NW2SE) {
			start = new IndexPair(startingLocation.rowIndex + index, startingLocation.colIndex + index);
			end = new IndexPair(startingLocation.rowIndex + index + needle.length() - 1,
					startingLocation.colIndex + index + needle.length() - 1);
		} else if (direction == DIRECTION_NE2SW) {
			start = new IndexPair(startingLocation.rowIndex + index, startingLocation.colIndex - index);
			end = new IndexPair(startingLocation.rowIndex + index + needle.length() - 1,
					startingLocation.colIndex - index - needle.length() + 1);
		}

		if (isReversed) {
			System.out.println(placeHolder + " " + end + " " + start);
			// System.out.println(new StringBuffer(needle).reverse() + " " + end + " " +
			// start);

		} else {
			System.out.println(placeHolder + " " + start + " " + end);
		}

	}
}
