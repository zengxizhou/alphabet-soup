package com.earth.dto;

import java.util.ArrayList;

public class Input {
	public int rowSize;
	public int colSize;
	public char[][] puzzle;

	/**
	 * Grid puzzle words in 4 directions are cashed. Directions include horizontal, vertical, Northwest to South east, and North east to South west.
	 */
	public ArrayList<String>[] puzzleCache = new ArrayList[4];
	
	/**
	 * Where the first letter in the puzzleCache word is located, zero based index pair.
	 */
	public ArrayList<IndexPair>[] locationCache = new ArrayList[4];

	/**
	 * words to be found
	 */
	public ArrayList<String> words;
}
