package com.earth.dto;

public class IndexPair {
	


	public  int rowIndex;
	public  int colIndex;
	
	public IndexPair(int i, int j) {
		this.rowIndex = i;
		this.colIndex = j;
	}
	public String toString() {
		return rowIndex+":"+colIndex;
	}
}
