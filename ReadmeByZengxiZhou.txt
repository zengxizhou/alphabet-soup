The project is developed by creating a simple Maven project inside an Eclipse IDE.
The main java class, com.earth.parser.Driver. 

To run it using mvn command line,
1) cd to the Maven project directory where pom.xml is located
2) mvn compile
3)mvn exec:java -Dexec.mainClass="com.earth.parser.Driver" -Dexec.args="<input file path>"
  For example, on my windows box, I use: mvn exec:java -Dexec.mainClass="com.earth.parser.Driver" -Dexec.args="'C:\Users\zengxi zhou\eclipse-workspace\wordsearch\src\main\resources\input.txt'"


I did not do any formal "Mock" or Junit testing as some production projects would do. However,  I tried to validate my code first by using the example provided.
I also created my own testing input files for validation of my code, trying to cover as many cases as possible.

Zengxi Zhou
zengxiz@yahoo.com